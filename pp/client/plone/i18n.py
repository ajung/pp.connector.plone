# -*- coding: utf-8 -*-

################################################################
# pp.client-plone
# (C) 2013-2019,  ZOPYX, D-72074 Tuebingen, www.zopyx.com
################################################################

try:
    from zope.i18nmessageid import MessageFactory
    MessageFactory = MessageFactory('pp.client-plone')
except ImportError:
    pass
