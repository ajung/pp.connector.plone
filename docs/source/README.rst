.. Produce & Publish Plone Client Connector documentation master file, created by
   sphinx-quickstart on Sun Nov 13 15:03:42 2011.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Produce & Publish Plone Client Connector
========================================

The Produce & Publish Plone Client connector integrates the Plone
CMS with the Produce & Publishing platform and supports the
generation of PDF (requires other external PDF converters).

Requirements
------------

- Plone 5.2 (Python 2.7 or Python 3.6+)

Compabitility
-------------

Use `pp.client-plone` for compatibility with Plone 4.X, 5.0 and 5.1.

Documentation
-------------

Primary documentation: http://pythonhosted.org/pp.connector.plone

Produce & Publish website: http://www.produce-and-publish.info

Source code
-----------
See https://bitbucket.org/ajung/pp.connector.plone

Bugtracker
----------
See https://bitbucket.org/ajung/pp.connector.plone/issues

Licence
-------
Published under the GNU Public Licence Version 2 (GPL 2)

Author
------
| Andreas Jung/ZOPYX
| Hundskapfklinge 33
| D-72074 Tuebingen, Germany
| info@zopyx.com
| www.zopyx.com



